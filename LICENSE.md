# Chat-Server

__This work__ is licensed under the <u>Creative Commons Attribution-ShareAlike 4.0 International License</u>.

To view a copy of this license, visit [creativecommons.org](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to _Creative Commons, PO Box 1866, Mountain View, CA 94042, USA_.

# Dependencies

* [nhooyr/websocket](https://github.com/nhooyr/websocket)
* [uber-go/zap](https://github.com/uber-go/zap)

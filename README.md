# About

This is a server for my networking final that serves to deliver messages between clients. The __the messages are not stored__ on the server-side once all clients receive the message.

* At that point, the message is deleted locally and only exists client-side.
* This is in case cryptography is implemented— it would defeat the purpose of encrypting messages if they were to be indefinitely stored in a central location.
* Additionally, it would not be too hard to fork this repository and set up dedicated \<message_cache> servers that could handle loads like a print queue. This particular server does not behave this way, however.

This server uses WebSockets, and so the proper address for connections to this server is as follows: `ws://<ip_address>:<port>/chat-server`.

Written in Neovim with `coc.nvim`.

## Build Instructions

1. Run the [build script](build/go.sh) at the root of the project.

Remember to configure your firewall to allow access to any ports that might be blocked. In the `chat-client` repository, there is a shell script for `iptables` users that will do it.

## Dependencies

* [Go](golang.org)
	> __IMPORTANT:__ Make sure to have `$GOPATH` set up on the machine that will host the client.

# Documentation

![Class Diagram](./class_diagram.png "`chat-server` Class Diagram")

## Data

### \<client_name> →

`<message_id>` | `-` | `_` | `.`

### \<client_session> →

* Result of `openssl rand -base64 25` in commandline.

### \<hour> →

`00` | `<month>` | `13` | `14` | `15` | `16` | `17` | `18` | `19` | `20` | `21` | `22` | `23`

### \<message_id> →

`<minute_y>`

| `a` | `b` | `c` | `d` | `e` | `f` | `g` | `h` | `i` | `j` | `k` | `l` | `m` | `n` | `o` | `p` | `q` | `r` | `s` | `t` | `u` | `v` | `w` | `x` | `y` | `z`

| `A` | `B` | `C` | `D` | `E` | `F` | `G` | `H` | `I` | `J` | `K` | `L` | `M` | `N` | `O` | `P` | `Q` | `R` | `S` | `T` | `U` | `V` | `W` | `X` | `Y` | `Z`

### \<minute> →

`<minute_x><minute_y>`

### \<minute_x> →

`0` | `1` | `2` | `3` | `4` | `5`

### \<minute_y> →

`<minute_x>` | `6` | `7` | `8` | `9`

### \<month> →

`01` | `02` | `03` | `04` | `05` | `06` | `07` | `08` | `09` | `10` | `11` | `12`

### \<room_id> →

`<message_id>`

### \<room_name> →

`<client_name>`

### \<second> →

`<minute>`

### \<year> →

`<minute_y>` | `<year><year>`

* A `<year>` should not be less than 2020, as 2020 is the smallest possible year we could have (since this application did not exist before 2020).

## Directories

### \<room_dir> →

```tree
<room_id>/
├── <message_cache.json>
└── <room.json>
```

### \<web_dir> →

```tree
web/
├── rooms/{
│	├── <room_dir>/}
│	└── <room_dir>/
├── <clients.json>
└── <queue.json>
```

## Files

### \<clients.json> →

```json
{
	<client_session_maps>
}
```

### \<message_cache.json> →

```json
{
	<message_id_maps>
}
```

### \<queue.json> →

```json
<queue>
```

### \<room.json> →

```json
<room>
```

## JSON

### \<client> →

```json
{
	"name": "<client_name>",
	"session": "<client_session>"
}
```

### \<client_names> →

`"<client>.<client_name>"` | `"<client_names>", "<client_names>"`

### \<client_session_maps> →

`"<client>.<client_name>": "<client>.<client_session>"` | `<client_session_maps>, <client_session_maps>`

### \<message> →

```json
{
	"body": "<message_body>",
	"client": "<client_name>",
	"time": "<year>-<month>-<day>_<hour>-<minute>-<second>"
}
```

### \<message_dequeued> →

* Messages are dequeued in reference to some `<message_queued>`.

```json
{
	"room": "<room_id>",
	"message": <message>
}
```

### \<message_ids> →

`"<message_id>"` | `<message_ids>, <message_ids>`

### \<message_id_maps> →

`"<message_id>": <message>` | `<message_id_maps>, <message_id_maps>`

### \<message_queued> →

* `<message_id>` refers to some `<message>` that has been stored in the `<message_cache.json>`

```json
"<room_id>": [<message_ids>]
```

### \<messages_queued> →

`"<message_queued>"` | `<messages_queued>, <messages_queued>`

### \<queue> →

```json
{
	<room_queue_maps>
}
```

* The message contents are accessed via [./web/rooms](./web/rooms/)`/<room_id>/message_cache.json`::`"<message_queued>.<message_id>"`

### \<room> →

```json
{
	"name": "<room_name>",
	"private": false
}
```

* If `"private"` is `false`, anyone can access the room.

#### |

```json
{
	"clients": [
		<client_names>
	],
	"name": "<room_name>",
	"private": true
}
```

* `"clients"` is the list of `<client>.<client_name>`s that are allowed to read and send messages
	* Any `<client>.<client_names>` _not_ in `"clients"` won't even be notified of the room's existence.

### \<room_queue_maps> →

`"<client_name>": { <messages_queued> }` | `<room_queue_maps>, <room_queue_maps>`

### \<room_info> →

```json
{
	"id": "<room_id>",
	"name": "<room_name>"
}
```

* Client names are accessed via [./web/clients.json](./web/clients.json)::`"<client_name>"`

### \<room_infos> →

`<room_info>` | `<room_infos>, <room_infos>`

### \<socket_op> →

```json
{
	"op_type": ("get"|"ok"|"post"),
	"content_type": "<content_type>"
}
```

## Operations

* Waiting to assure Requests/Responses have been delivered/received is assumed.

### \<CLIENT_CONNECTED> →

```mermaid
sequenceDiagram
	participant C as Client
	participant S as Server

	Note over C,S: TCP Handshake & WebSocket upgrade.

	C ->> S: POST <client>

	opt <client_name> invalid
		S ->> C: 4000 Invlaid username
	end


	alt <client_session> is null
		alt <client_name> unused
			S ->> C: POST unique <client_session>
			S --> S: Add to <clients.json>
		else <client_name> used
			S ->> C: 4001 Username taken
		end
	else
		alt <client_session> not recognized
			S ->> C: 4003 Bad login
		else name:session map unmatched
			S ->> C: 4002 Never assigned
		else
			S ->> C: OK <client_session>
		end
	end

	S ->> C: POST <room>s
	C ->> C: Populate server <room> list

	C --> S: <FETCH>
```

### \<CLIENT_MESSAGE_SENT> →

```mermaid
sequenceDiagram
	participant C as Client
	participant S as Server
	participant R as Recipients

	Note over C,R: Client sends <message_dequeued> to Recipients via Server.

	C ->> S: POST <message_dequeued>

	opt Room !exists
		S ->> C: 4004 Bad target
	end

	alt Room visible to C
		opt <message_cache.json> full
			S ->> C: 4006 Capacity met
		end

		S --> S: Queue <message>

		S ->> R: OK <FETCH>

		Note over C,S: Sent <message>
		Note over S,R: Relayed <message>
	else Room !visible to C
		S ->> C: 4005 Unauthorized access

		Note over C,R: <message> not relayed
	end
```

### \<FETCH> →

```mermaid
sequenceDiagram
	participant C as Client
	participant S as Server

	Note over C,S: "OK <FETCH>" has been sent.

	loop until OK.
		C ->> S: GET <queue>
		alt <message_queued> for Client
			S --> S: Dequeue <message_queued>
			S ->> C: POST    <queue>
			S --> S: Remove  <message_queued>
		else no <message_queued>
			S ->> C: OK <queue>
			Note over C,S: BREAK
		end
	end

	Note over C: Ready for user
	Note over S: On standby
```

## WebSockets

Clients are given a 10 second timeout for all operations _besides_ a passive connection.
	* This entails operations that the server has solicited from the client.

* All __acknowledgements__ are `<socket_op>`s with `"op_type": "ok"`.
* All __requests__ are `<socket_op>`s with `"op_type": "get"`.
* All __responses/user-events__ follow this format:
	```json
	{
		"content": <content>,
		"operation": <socket_op>
	}
	```
	where `<socket_op>` has `"op_type": "post"`.

	* This is simply to prevent the repetition of data.

### \<client> →

* `<content>` → `<client>`
* `<content_type>` → `client`

* `POST`ed by client.

### \<client_session> →

* `<content>` → `<client_session>`
* `<content_type>` → `client_session`

* `OK`d by server.
* `POST`ed by server.
* Statuses `4000`–`4003` may be issued by the server.

### \<FETCH> →

* `<content_type>` → `fetch`

* `OK`d by server.

### \<message> →

* `<content>` → `<message_dequeued>`
	* Yes, the content is not meant to be a `<message>`. The server must know what room the message is sent to.
* `<content_type>` → `message`

* `POST`ed by client.
* Status `4004`—`4006` may be issued by the server.

### \<queue> →

* `<content>` → `<message_dequeued>`
* `<content_type>` → `queue`

* `GET`d by client.
* `OK`d by server.
* `POST`ed by server.

### \<room>s →

* `<content>` →
	```json
	[
		<room_infos>
	]
	```
	* Only sends `<room_info>`s where the `<client_name>` has access to the corresponding `<room>`.
* `<content_type>` → `rooms`

* `POST`ed by server.

# Usage

* Run [mkroom](./bin/mkroom.go) to create a new room from the template.
	* Use the `-h` flag to view flags and options.
* Run [server-start](./bin/server-start) to start the server.
	* Use the `-h` flag to view flags and options.
	* You must have a directory (known as the "`<web_dir>`") that is dedicated to files for the server.
		* The application is capable of creating necessary files, outside of `<room_dir>`s, which are created by `mkroom`.

#!/usr/bin/env bash

PROJ_PATH="$GOPATH/src/gitlab.com/Iron_E/networking-final/chat-server"

cmds=(mkroom start-server)
dir_dist="$PROJ_PATH/bin"
dir_src="$PROJ_PATH/cmd"

for c in ${cmds[*]} ; do
	go build -o "$dir_dist/$c" "$dir_src/$c/main.go"
done


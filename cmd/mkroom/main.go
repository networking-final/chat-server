package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/files"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/validity"
)

// Entry point for creating a new room with valid JSON template.
func main() {
	optionRoomIdPtr := flag.String("id", "_", "The ID of the room.")
	optionRoomNamePtr := flag.String("name", "_", "The name of the room.")
	optionWebDir := flag.String("web-dir", "", "The path to the directory that contains the <web_dir>")
	flag.Parse()

	// If the data is not correct ("_" being the default value)
	if err := testValidity(*optionRoomIdPtr, *optionRoomNamePtr); err != nil {
		panic(err)
	}

	// Get the "web/rooms" dir.
	roomDirPath := files.RoomDirPath(*optionWebDir)

	// Make sure that the "web/rooms" dir exists.
	if _, err := os.Stat(roomDirPath); os.IsNotExist(err) {
		os.MkdirAll(roomDirPath, 0777);
	}

	// Read out the nodes from the "web/rooms" dir
	if nodes, err := ioutil.ReadDir(roomDirPath); err != nil {

		panic(err)

	} else {

		for i := range nodes {

			if nodeIdMatch(nodes[i], *optionRoomIdPtr) {

				panic(errors.New("Pre-existing room has that ID."))
			}
		}

		if err := mkdir( marshal.RoomId(*optionRoomIdPtr),marshal.RoomName(*optionRoomNamePtr),*optionWebDir ); err != nil {

			panic(err)
		}
	}
}

// Create directory for <room>.
func mkdir(roomId marshal.RoomId, roomName marshal.RoomName, webDir string) error {
	// Assuming the program has not panicked yet, create the folder.
	dir := path.Join( files.RoomDirPath(webDir),string(roomId) )

	// Initialize the structs.
	room := marshal.RoomPrivate(marshal.ClientNames{}, roomName)

	// Create the <room.json> struct.
	roomFile := files.RoomWithContents(room, roomId)

	// Create the [dir] and files inside [dir].
	if err := os.Mkdir(dir, 0777); err != nil {

		return err
	}

	if err := roomFile.WriteToFile(webDir); err != nil {

		return err
	}

	if _, err := os.Create(fmt.Sprintf("%s/message_cache.json", dir)); err != nil {

		return err
	}

	return nil
}

// Make sure the two room's don't have the same id.
func nodeIdMatch(roomNode os.FileInfo, roomId string) bool {
	if roomNode.IsDir() {

		// If there is a Dir that is the same name as the <room_id> passed in.
		if roomNode.Name() == roomId {

			return true
		}
	}
	return false
}

// Make sure the <room> has a valid ID.
func testValidity(roomId string, roomName string) error {
	if roomId == "_" || !validity.TestForValidRoomId(marshal.RoomId(roomId)) {

		return fmt.Errorf("Please enter a valid <room_id>.")

	} else if roomName == "_" || !validity.TestForValidRoomName(marshal.RoomName(roomName)) {

		return fmt.Errorf("Please enter a valid <room_name>.")
	}

	return nil
}

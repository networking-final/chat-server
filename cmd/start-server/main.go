package main

import (
	"flag"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws"
	"gitlab.com/Iron_E/networking-final/chat-server/internal/shelve"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/dirs"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

// Entry point for serving the chat application.
func main() {
	if logger, err := zaputil.NewProduction("main()"); err != nil {

		panic(err)

	} else {
		defer logger.Sync()

		// Set flags
		optionPortPtr := flag.Int("port", 0, "The port number you wish to start the server on.")
		optionWebDir := flag.String("web-dir", "", "The path to the directory that contains the <web_dir>")
		flag.Parse()

		// Set the seed for later session generations.
		rand.Seed(time.Now().UnixNano())

		// Read local files
		if webDir, err := dirs.WebFromFile(*optionWebDir); err != nil {

			panic(err)

		} else {

			// Create a server and listener
			listener, server := ws.Prepare(*optionPortPtr, webDir)

			defer func() {// †
				// Remember to close the server.
				if err := server.Close(); err != nil {

					logger.Error("There was an error while killing the server.",
						zap.Error(err),
					)

				} else {

					logger.Info("Server closed.")
				}

				// Remember to close the listener.
				if err := (*listener).Close(); err != nil {

					logger.Error( "Error while closing listener.",
						zap.Error(err),
					)

				} else {

					logger.Info("Listener closed.")
				}

				// Remember to store the [webDir] back on the disk.
				logger.Info("Shelving session.")
				shelve.Shelve(webDir, *optionWebDir)
				logger.Info("\tSession shelved.")

			}()// ‡

			// Start the server with the options provided on a separate thread.
			go func() {
				if err := server.Serve(*listener); err != nil {

					logger.Error( "Server closed.",
						zap.Error(err),
					)
				}
			}()

			listenSig()
		}
	}

}

// Listen for when the user presses Ctrl-C and kill the server gracefully.
// NOTE: subsequent Ctrl-C interrupts will not be handled.
func listenSig() {
	if logger, err := zaputil.NewProduction("listenSig()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		// Create a channel and WaitGroup for a Ctrl-C event.
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)


		// Wait for one signal to occur.
		logger.Info("Listening for SIGINT/TERM signals.")
		<-sigs

		// Close the channel.
		logger.Info("SIGINT/TERM signal received.")
		signal.Stop(sigs)
		close(sigs)
	}
}

module gitlab.com/Iron_E/networking-final/chat-server

go 1.14

require (
	go.uber.org/zap v1.14.1
	nhooyr.io/websocket v1.8.4
)

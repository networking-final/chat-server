package broadcast

import (
	"encoding/json"
	"sync"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/socket_op"
	"nhooyr.io/websocket"
)

/*
 * STRUCTS
 */

// Map active connections to a channel for receiving events.
type Broker struct {
	mutController sync.RWMutex
	subscribers   map[*websocket.Conn]chan<- []byte
}

/*
 * METHODS
 */

// Add a subscriber channel only if it is not already there.
// Return a boolean representing the success of the operation (whether or not there was an addition to the map) and also the newly added channel (or nil if no channel was added).
func (b *Broker) PutSubscriberIfAbsent(conn *websocket.Conn) (<-chan []byte, bool) {
	// Don't allow any other threads to read nor write while this method is running.
	b.mutController.Lock()
	defer b.mutController.Unlock()

	// Make sure that the subscribers map has been initialized.
	if b.subscribers == nil {
		b.subscribers = make(map[*websocket.Conn]chan<- []byte)
	} 

	// Attempting to subscribe with a non-unique identifier.
	if _, keyExists := b.subscribers[conn]; keyExists {
		return nil, false
	// Identifier is unique.
	} else {
		// Create a new channel.
		// We can't just assign directly because `<-chan` and `chan<-` are incompatible types. However, they can both be derived from a `chan`.
		channel := make(chan []byte)
		b.subscribers[conn] = channel
		return channel, true
	}
}

// Let all clients (except [from]) know that they should <FETCH>.
func (b *Broker) Fetch(from *websocket.Conn) ([]*websocket.Conn, error) {
	if jsonBytes, err := json.Marshal(socket_op.Fetch); err != nil {
		// Don't publish malformed JSON.
		return nil, err
	} else {
		// Publish and return nil in place of an error.
		return b.publish(from, jsonBytes), nil
	}
}

// Publish a message on every subscriber channel except [from] (the one that started this event).
// Returns a list of clients that did not receive this publish.
// Note: `*websocket.Conns` is not used here because there is explicit intent to return a slice of `Clientconn`s, regardless of what underlying definition `*websocket.Conns` has.
func (b *Broker) publish(from *websocket.Conn, msg []byte) []*websocket.Conn {
	// Create a slice of clients that did not receive the publish event.
	missed := make([]*websocket.Conn, 0, len(b.subscribers))

	// Don't allow writing but allow other threads to read.
	b.mutController.RLock()
	defer b.mutController.RUnlock()

	// Iterate over all of the subscribers.
	for conn, subscriber := range b.subscribers {
		// Don't push a message back where it came from.
		if conn != from {
			select {
				// If the subscriber is accepting messages.
				case subscriber <- msg:
					break
				// If the subscriber is not accepting messages, drop the message.
				// All messages are queued anyway, so it can get picked up again later.
				default:
					missed = append(missed, conn)
					break
			}
		}
	}

	return missed
}

// Remove a subscriber channel.
// Returns `true` if there was a removal performed, and `false` if nothing happened.
func (b *Broker) RemoveSubscriber(conn *websocket.Conn) bool {
	// Don't allow any other threads to read nor write while this method is running.
	b.mutController.Lock()
	defer b.mutController.Unlock()

	if _, keyExists := b.subscribers[conn]; keyExists {
		close(b.subscribers[conn])
		delete(b.subscribers, conn)
		return true
	} else {
		return false
	}
}

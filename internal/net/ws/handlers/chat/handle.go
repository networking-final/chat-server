package chat

import (
	"context"
	"encoding/json"
	"net/http"
	"runtime"
	"time"

	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/broadcast"
	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/handlers"
	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/handlers/chat/operations"
	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/handlers/chat/responses"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/dirs"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/socket_op"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

// Handle connections to the server's chat address.
func NewHandleFunc(broker *broadcast.Broker, webDir *dirs.WebDir) func(w http.ResponseWriter, r *http.Request) {
	if logger, err := zaputil.NewProduction("NewChatHandlerFunc()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		// Create a new anonymous-inner-function based on the arguments provided, and return it.
		return func(w http.ResponseWriter, r *http.Request) {
			// Upgrade to a WebSocket connection.
			logger.Info( "HTTP Received. Upgrading to WebSocket." )
			connection, err := websocket.Accept(w, r, &websocket.AcceptOptions{
				// Normally this would be a very bad option to have.
				// However, because of how this server is intended to be used (i.e. with any kind of client that adheres to the spec), it is being enabled.
				// Changing it to false would make it more secure, however it would prevent the sort of "propping up" of the server that I had envisioned.
				InsecureSkipVerify: true,
			})

			// Check for errors and log them if there are any.
			if err != nil {

				logger.Error("Error upgrading connection:",
					zap.Error(err),
				)

			} else { // If there were no errors, continue normally

				// Make sure the connection is closed
				defer func() {
					logger.Info("Closing `connection`")
					broker.RemoveSubscriber(connection)
					if err := connection.Close(websocket.StatusAbnormalClosure, "The server encountered an error."); err != nil {
						logger.Error("Error on websocket close:",
							zap.Error(err),
						)
					}
				}()

				logger.Info("...upgraded.")

				logger.Info("Created timeout context. Current threads:",
					zap.Int( "Thread count", runtime.NumGoroutine() ),
				)

				// Remember to release the resources associated with the client connection.
				defer func() {
					logger.Info("Cancelling 'impatient' context.")
				}()

				// Define queue for information to be written.
				writeQueue := make(chan interface{})
				defer close(writeQueue)

				// Create information for the handlers.
				// NOTE: It should not be reset each "for", so it should not be put there.
				info := &handlers.Information{
					Broker: broker,
					Conn: connection,
					WriteQueue: writeQueue,
					WebDir: webDir,
				}

				// Define writer for json. This way the write lock is only acquired in one place at a time.
				go func(buffer <-chan interface{}) {
					for toWrite := range buffer {
						// Create a new context.
						writeCtx, writeCtxCancel := context.WithTimeout(
							context.Background(), 10 * time.Second,
						)

						// Write to client.
						err := wsjson.Write(writeCtx, connection, toWrite)

						// Cancel the context.
						writeCtxCancel()

						// Var for result text.
						zapFields := []zap.Field{
							zap.String( "Client Name",string(info.GetClient()) ),
							zap.String( "IP",r.RemoteAddr ),
							zap.Any( "JSON",toWrite ),
						}

						// Determine result text.
						if err != nil {
							// Log Result
							logger.Error("There was an error sending JSON to client.",
								append(zapFields, zap.Error(err))...,
							)
						} else {
							// Log Result
							logger.Info("JSON successfully sent to client.", zapFields...)
						}

					}
				}(writeQueue)

				// Loop to keep the connection alive.
				for {

					// Create variable to hold unmarshalled JSON.
					jsonMap := make(map[string]interface{})

					// Read from the socket.
					if err := wsjson.Read(context.Background(), connection, &jsonMap); err != nil {

						// Log the error and close the connection.
						logger.Error( "Error reading JSON:",zap.Error(err) )

						// Track whether or not the ping succeeded.
						pingSuccess := false

						// Create context for ping.
						pingCtx, pingCtxCancel := context.WithTimeout(
							context.Background(), 10 * time.Second,
						)

						for i := 0; i < 5; i++ {
							if pingErr := connection.Ping(pingCtx); pingErr == nil {
								logger.Info("Ping succeeded, maintaining connection.")
								pingSuccess = true
								break
							} else {
								logger.Warn("Ping failed, trying again...",
									zap.String( "Client Name",string(info.GetClient()) ),
									zap.Int("Ping Attempt #", i),
								)
							}
						}
						pingCtxCancel()

						// If none of the pings succeed, then kill the connection to the client.
						if !pingSuccess {
							logger.Warn("Could not ping client. Closing connection.",
								zap.String( "Client Name",string(info.GetClient()) ),
								zap.String( "IP",r.RemoteAddr ),
							)
							// Close the connection if it has not already been closed.
							connection.Close(websocket.StatusNoStatusRcvd, "Could not ping client.")
							// break out of the for loop.
							break

						} else {

							logger.Info("Ping succeeded. Continuing to listen...",
								zap.String( "Client Name",string(info.GetClient()) ),
								zap.String( "IP",r.RemoteAddr ),
							)
							continue
						}

					} else { // Otherwise, continue normally.

						// Attempt to marshal the JSON map into `[]byte`s so that the initializers can read it.
						if jsonBytes, err := json.Marshal(jsonMap); err != nil {

							logger.Error("Error marshalling JSON map into []byte.",
								zap.Error(err),
							)

						} else { // Only continue if it was successful.

							// Test what type `unmarshalledJson` is.
							if socket_op.IsOperation(jsonMap) {

								// Unmarshal the JSON into a format we can operate on.
								clientRequest := new(socket_op.Operation)
								json.Unmarshal(jsonBytes, &clientRequest)

								// Handle the operation.
								operations.Handle(*clientRequest, info)

							// Use `else if` just in case there is bad JSON being sent to the server.
							} else if socket_op.IsResponse(jsonMap) {

								// Unmarshal the JSON into a format we can operate on.
								clientMsg := new(socket_op.Response)
								json.Unmarshal(jsonBytes, clientMsg)

								// Handle the response.
								responses.Handle(*clientMsg, info)

							// If the JSON type is unknown.
							} else {

								logger.Warn("Client tried to send bad JSON.",
									zap.String("Client Addr", r.RemoteAddr),
									zap.Any("JSON", jsonMap),
								)
							}
						}
					}
				}
			}
		}
	}
}

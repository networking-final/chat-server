package operations

import (
	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/handlers"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/socket_op"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

// Determine what kind of [o] there is, and use [info] to handle it.
func Handle(o socket_op.Operation, info *handlers.Information) {
	if logger, err := zaputil.NewProduction("handleResponse()"); err != nil {
		panic(err)
	} else {
		/* NOTE: See the server specification for more information on these events. */

		// Select the operation type.
		switch o.OpType {

			case socket_op.OP_TYPE_ACKNOWLEDGE:
				// There are currently no known client acknowledgements in the specification.
				break
				
			case socket_op.OP_TYPE_REQUEST:
				// Select the content type.
				switch o.ContentType {
					// Client connecting to server.
					case socket_op.TYPE_QUEUE:
						handleOpTypeMsgDq(info)
						break

					// Unrecognized response.
					default:
						logger.Warn("Malformed <content_type>.",
							zap.String( "Client",string(info.GetClient()) ),
							zap.String( "<content_type>",o.ContentType ),
						)
						break
				}
				
			default:
				logger.Warn("Malformed <op_type>",
					zap.String( "Client",string(info.GetClient()) ),
					zap.String( "<content_type>",o.OpType ),
				)
				break
		}
	}
}

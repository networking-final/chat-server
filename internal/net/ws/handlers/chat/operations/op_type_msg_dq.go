package operations

import (
	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/handlers"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/socket_op"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

// If a <socket_op> response's <content_type> is "client", run this handler.
func handleOpTypeMsgDq(info *handlers.Information) {
	if logger, err := zaputil.NewProduction("handleOpTypeMsgDq()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		logger.Info("Client has requested the messages in its queue.",
			zap.String( "Client",string(info.GetClient()) ),
		)

		if info.GetWebDir().GetQueue().IsMsgForClient(info.GetClient()) {

			logger.Info("There are mesasges in the client's queue.",
				zap.String( "Client",string(info.GetClient()) ),
			)

			// Iterate over all of the rooms that have messages for [info.Client].
			for roomId := range info.GetWebDir().GetQueue().Get(info.GetClient()) {

				logger.Info("Reading messages for client in one of their rooms.",
					zap.String( "Client",string(info.GetClient()) ),
					zap.String( "Room ID",string(roomId) ),
				)

				// Make sure that there actually is a room for the corresponding <queue.json> entry.
				if room := info.GetWebDir().GetRoom(roomId); room != nil {

					// Loop while there are message ids left.
					for {
						msgIds := info.GetWebDir().GetQueue().Get(info.GetClient())[roomId]

						if len(msgIds) < 1 { break }

						logger.Info("There are messages left for the client.",
							zap.Int("Messages Left", len(msgIds)),
						)

						dequeuedResponse := socket_op.NewResponse(
							// Unpack the first queued message in the proper format for the client. †
							marshal.MessageDequeued{Id: roomId, Msg: room.GetCache().Get(msgIds[0])},
							socket_op.TYPE_QUEUE, // ‡
						)

						// Add the message to the write queue.
						info.GetWriteQueue() <- dequeuedResponse

						// Dequeue the first queued message.
						logger.Info("Next available message queued in writer. Removing first message from queue.",
							zap.String( "Client",string(info.GetClient()) ),
							zap.String( "Message ID",string(msgIds[0]) ),
							zap.String( "Message ID",string(msgIds[0]) ),
							zap.Any( "Response",dequeuedResponse ),
						)

						info.GetWebDir().GetQueue().RemoveFirst(info.GetClient(), roomId)
					}
				// There was a <queue.json> entry for a <room_id> that does not have a corresponding <room_dir>
				} else {

					logger.Error("There was somehow a <room_id> in <queue.json> that does not have a corresponding <room_dir>",
						zap.String( "Client",string(info.GetClient()) ),
						zap.String( "Room ID",string(roomId) ),
					)
				}
			}
		}

		// Send the OK to the client that they may render any new messages from the server.
		info.GetWriteQueue() <-  socket_op.NewAcknowledgement(socket_op.TYPE_QUEUE)

		logger.Info("'OK <queue>' socket_op queued in writer.",
			zap.String( "Client",string(info.GetClient()) ),
		)
	}
}

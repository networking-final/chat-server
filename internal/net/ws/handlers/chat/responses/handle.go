package responses

import (
	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/handlers"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/socket_op"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

// Determine what kind of [r] there is and use [info] to handle it.
func Handle(r socket_op.Response, info *handlers.Information) {
	if logger, err := zaputil.NewProduction("handleResponse()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		/* NOTE: See the server specification for more information on these events. */

		// Select the operation type.
		switch r.Op.OpType {
			case socket_op.OP_TYPE_RESPONSE:
				switch r.Op.ContentType {
					// Client connecting to server.
					case socket_op.TYPE_CLIENT:
						handleOpTypeClient(r, info)
						break

					// Client sending a message to the server.
					case socket_op.TYPE_MSG:
						handleOpTypeMsg(r, info)
						break

					// Unrecognized <content_type>
					default:
						logger.Warn("Malformed <content_type>.",
							zap.String( "Client",string(info.GetClient()) ),
							zap.String( "<content_type>",r.Op.ContentType ),
						)
						break
				}

			// Unrecognized <op_type>
			default:
				logger.Warn("Malformed <op_type>.",
					zap.String( "Client",string(info.GetClient()) ),
					zap.String( "<op_type>",r.Op.OpType ),
				)
				break
		}
	}
}

package responses

import (
	"encoding/json"
	"errors"

	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/handlers"
	"gitlab.com/Iron_E/networking-final/chat-server/internal/random"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/socket_op"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/validity"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
	"nhooyr.io/websocket"
)

// If a <socket_op> response's <content_type> is "client", run this handler.
func handleOpTypeClient(r socket_op.Response, info *handlers.Information) {
	if logger, err := zaputil.NewProduction("handleOpTypeClient()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		// There were errors marshalling the map object back into a []byte.
		if jsonBytes, err := json.Marshal(r.Content); err != nil {

			logger.Error("Could not remarshal response <content>.",
				zap.Any("Content", r.Content),
				zap.Error(err),
			)

		// There were no errors marshalling the map object back into a []byte.
		} else {

			// Unmarshal the JSON into
			client := marshal.NewClient()
			json.Unmarshal(jsonBytes, &client)

			// Assure the client's username fits the definition of a <client_name> from the server spec.
			// The username contained illegal characters.
			if !validity.TestForValidClientName(client.GetName()) {

				info.GetConn().Close(
					websocket.StatusCode(4000),
					"Username contains illegal characters.",
				)

			// The client's username only contained legal characters.
			} else {

				clientVerified := new(bool)
				*clientVerified = false

				// The client believes it is brand new and is requesting a session.
				if client.GetSession() == "" {

					// Determine if the username is available or not.
					usernameAvailable := true

					if info.GetWebDir().GetClients().SessionExists(client.GetName()) {

							usernameAvailable = false
					}

					// The username is free.
					if usernameAvailable {

						// Create a new session.
						newSession := random.RandomSession(25)
						postNewSession := socket_op.NewResponse(
							newSession,
							socket_op.TYPE_CLIENT_SESSION,
						)

						// Send the new session. If there is an error, print it.
						info.GetWriteQueue() <- postNewSession

						// Verify the client
						*clientVerified = true
						logger.Info( "Response queued in writer. Client is now verified. Mapping the client.",
							zap.String("Client", string(client.GetName())),
							zap.Any("Session", postNewSession.Content),
						)

						// Update client's information.
						client.SetSession( marshal.ClientSession(newSession) )

						// Map the new client.
						info.GetWebDir().Clients.Put(client.GetName(), client.GetSession())

					// The username is taken.
					// This is like creating an account with a username that someone else already has.
					} else {

						info.GetConn().Close( websocket.StatusCode(4001),
							"The username you have selected is unavailable.",
						)
					}



				// Client session is not empty; the client believes it has connected before.
				} else {

					// Read data about the connecting client's history with the server.
					sessionExists := info.GetWebDir().Clients.SessionExists(client.GetName())

					// The server has a record of this client connecting before.
					if sessionExists {

						// The client's recorded session for the username specified and the server's stored session for the username specified are identical.
						if client.GetSession() == info.GetWebDir().GetClients().GetSession(client.GetName()) {

							// Tell the user that they are OK to connect to the server.
							// Even if a client doesn't listen and tries to connect anyway, the server will not register them as connected (and thusly they won't receive any messages.)
							allowUser := socket_op.NewAcknowledgement(socket_op.TYPE_CLIENT_SESSION)

							// If there was an error, do not mark the <client> as verified.
							info.GetWriteQueue() <- allowUser

							logger.Info( "Response queued in writer. Client is now verified.",
								zap.String("Client", string(client.GetName())),
								zap.Any("Session", string(client.GetSession())),
							)
							*clientVerified = true

						// The client's proposed session does not match the internal one.
						// This is equivalent to having a correct username and an invalid password.
						} else {

							info.GetConn().Close( websocket.StatusCode(4003),
								"Username:Session combination invalid.",
							)
						}

					// The server has never seen this <client> before, yet it has a <client_session> assigned.
					// This is like trying to login to a website you have no account with.
					} else {

						info.GetConn().Close( websocket.StatusCode(4002),
							"The username is not known, yet the client has a session key for it. This could be because you have mistyped your username.",
						)
					}
				}

				// Define a listener thread so that when other clients send messages, it is there to receive.
				// If a client was not verified, there is no need to subscribe them to the messaging broker.
				//
				// This is like a user successfully signing in to a website and beginning to receive push notifications.
				if *clientVerified {
					
					// Update the client for [info].
					info.SetClient(client.GetName())

					// Perform actions to get the client ready for interaction with the server.
					subscribeClient(info)
					sendRoomList(info)

					info.GetWriteQueue() <- socket_op.Fetch

				} else {
					session := info.GetWebDir().Clients.GetSession(client.GetName())
					logger.Info("Client was not verified. Dropping.",
						zap.String( "Client", string(client.GetName()) ),
						zap.String( "Session", string(session) ),
					)
					info.GetConn().Close(websocket.StatusAbnormalClosure, "Client was not verified.")
				}
			}
		}
	}
}

// Send (over [info.Conn]) the rooms from [info.WebDir] that should be visible to [info.Client].
// Uses [info.Ctx] to determine timeouts.
func sendRoomList(info *handlers.Information) {
	if logger, err := zaputil.NewProduction("sendRoomList()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		// Generate list of rooms for client.
		rooms := info.GetWebDir().RoomsFor(info.GetClient())

		// Send the list of rooms that the user can connect to.
		logger.Info("Sending client the list of rooms.",
			zap.String( "client",string(info.GetClient()) ),
			zap.String( "Session",string(info.WebDir.GetClients().GetSession(info.GetClient())) ),
			zap.Any("Rooms", rooms),
		)

		info.GetWriteQueue() <- socket_op.NewResponse(rooms,"rooms")
	}
}

// Subscribe some [info.Client] to a [info.Broker]. This spawns a goroutine that will notify the [info.Client] over the [info.Conn] to fetch new messages.
// Uses [info.Ctx] to determine timeouts.
func subscribeClient(info *handlers.Information) error {
	if logger, err := zaputil.NewProduction("acceptClientInfoAsValid()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		// Subscribe to the broker in a new thread.
		logger.Info("Client verification acknowledged. Subscribing to publisher.",
			zap.String( "Client",string(info.GetClient()) ),
			zap.String( "Session",string(info.WebDir.GetClients().GetSession(info.GetClient())) ),
		)

		// Do not resubscribe an existing client.
		if events, wasAddded := info.GetBroker().PutSubscriberIfAbsent(info.GetConn()); wasAddded {

			// Launch a new goroutine specifically to listen to the Broker for this client.
			go func() {
				// NOTE: There is only one kind of event that we can receieve as a broadcast, so it is not necessary to store it. 
				// Check the specification for more details.
				for range events {
					// Send this client a notification saying that they may retrieve a new message from the server.
					info.GetWriteQueue() <- socket_op.Fetch
					logger.Info("Queued notification of new message in writer.",
						zap.String( "Client",string(info.GetClient()) ),
						zap.String( "Session",string(info.GetWebDir().GetClients().GetSession(info.GetClient())) ),
					)
				}
			}()

		} else {

			return errors.New("The client was not subscribed.")
		}

		return nil
	}
}

package responses

import (
	"encoding/json"

	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/handlers"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/socket_op"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
	"nhooyr.io/websocket"
)

// If a <socket_op> response's <content_type> is "client", run this handler.
func handleOpTypeMsg(r socket_op.Response, info *handlers.Information) {
	if logger, err := zaputil.NewProduction("handleOpTypeMsg()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		// There were errors marshalling the map object back into a []byte.
		if jsonBytes, err := json.Marshal(r.Content); err != nil {

			logger.Error("Could not remarshal response <content>.",
				zap.Any("Content", r.Content),
				zap.Error(err),
			)

		// There were no errors marshalling the map object back into a []byte.
		} else {

			// Unmarshal the JSON into a <message_dequeued>.
			msgDq := new(marshal.MessageDequeued)
			json.Unmarshal(jsonBytes, msgDq)

			logger.Info("Unmarshalled JSON successfully.",
				zap.Any("<message_dequeued>", msgDq),
			)

			// Make sure that the client has not "pulled a fast one" by setting the "client" field to another username.
			msgDq.GetMsg().SetClient(info.GetClient())

			if !info.GetWebDir().RoomExists(msgDq.GetId()) {

				logger.Warn("Client tried to send message to room that does not exist.",
					zap.String( "Client Name",string(info.GetClient()) ),
					zap.String( "Room ID",string(msgDq.GetId()) ),
				)

				info.GetConn().Close(websocket.StatusCode(4004), "Message was sent to room that does not exist.")

			} else {

				// If the client does not have access to the room that the message was sent to.
				if info.GetWebDir().GetRoom(msgDq.GetId()).GetCfg().Get().VisibleToClient(info.GetClient()) {

					logger.Warn("Client tried to send message to room that they do not have access to.",
						zap.String( "Client Name",string(info.GetClient()) ),
						zap.String( "Room ID",string(msgDq.GetId()) ),
					)

					info.GetConn().Close(websocket.StatusCode(4005), "Message was sent to a room that client does not have permission to access.")

				// Client has sent the message to a room that should be visible to them.
				} else {

					// Try to queue the message
					if msgId, err := info.GetWebDir().GetRoom(msgDq.GetId()).GetCache().Put(msgDq.GetMsg()); err != nil {

						logger.Error("The room's <message_cache.json> is full.",
							zap.String( "Room ID",string(msgDq.GetId()) ),
							zap.Error(err),
						)

						info.GetConn().Close(websocket.StatusCode(4006), "The server has too many messages cached. Please contact the administrator.")

					} else {

						logger.Info("Added message to <message_cache.json>.",
							zap.String( "From Client",string(info.GetClient()) ),
							zap.String( "Message ID",string(msgId) ),
						)

						var clients []marshal.ClientName

						// If the room is private, read the defined clients and only queue the message for them.
						if info.GetWebDir().GetRoom(msgDq.GetId()).GetCfg().Get().IsPrivate() {

							logger.Info("Room was private. Grabbing clients...",
								zap.String( "Room ID",string(msgDq.GetId()) ),
							)

							clients = info.GetWebDir().GetRoom(msgDq.GetId()).GetCfg().Get().GetClients()

						// If the room is public, queue the message for all clients.
						} else {

							logger.Info("Room was public. Grabbing all known clients...",
								zap.String( "Room ID",string(msgDq.GetId()) ),
							)

							clients = info.GetWebDir().GetClients().Names()
						}

						// Make sure we have clients
						// NOTE: we should always pass at this point.
						if clients != nil {

							// Iterate over the clients that were determined previously.
							for i := range clients {

								// Queue the message for each client besides the sender.
								if clients[i] != info.GetClient() && clients[i] != "" {
									logger.Info("Marking message as queued for other client.",
										zap.String( "Client",string(info.GetClient()) ),
										zap.String( "Message ID",string(msgId) ),
										zap.String( "Other Client",string(clients[i]) ),
										zap.String( "Room ID",string(msgDq.GetId()) ),
									)
									info.GetWebDir().GetQueue().Put(clients[i], msgDq.GetId(), msgId)
								}
							}

							// Announce the new message.
							info.GetBroker().Fetch(info.GetConn())

						// If there check fails, log the error.
						} else {

							logger.Error("The names of the clients connected to the server was 'nil', yet there needed to be a client connected in order to produce this message.")
						}
					}
				}
			}
		}
	}
}

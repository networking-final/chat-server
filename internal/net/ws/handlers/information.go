package handlers

import (
	"sync"

	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/broadcast"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/dirs"
	"nhooyr.io/websocket"
)

/*
 * STRUCT
 */

// Information necessary to interact with a connected client on multiple threads.
type Information struct {
	mutController sync.RWMutex
	Broker *broadcast.Broker
	Client  marshal.ClientName
	Conn   *websocket.Conn
	WebDir *dirs.WebDir
	WriteQueue chan<- interface{}
}

/*
 * METHODS
 */

// A thread-safe method to get the [Broker] member.
func (info *Information) GetBroker() *broadcast.Broker {
	info.mutController.RLock()
	defer info.mutController.RUnlock()

	return info.Broker
}

// A thread-safe method to get the [Client] member.
func (info *Information) GetClient() marshal.ClientName {
	info.mutController.RLock()
	defer info.mutController.RUnlock()

	return info.Client
}

// A thread-safe method to get the [Conn] member.
func (info *Information) GetConn() *websocket.Conn {
	info.mutController.RLock()
	defer info.mutController.RUnlock()

	return info.Conn
}

// A thread-safe method to get the [WebDir] member.
func (info *Information) GetWebDir() *dirs.WebDir {
	info.mutController.RLock()
	defer info.mutController.RUnlock()

	return info.WebDir
}

// A thread-safe method to get the [WebDir] member.
func (info *Information) GetWriteQueue() chan<-interface{} {
	info.mutController.RLock()
	defer info.mutController.RUnlock()

	return info.WriteQueue
}

// A thread-safe method to set the [Client] member.
func (info *Information) SetClient(name marshal.ClientName) {
	info.mutController.Lock()
	defer info.mutController.Unlock()

	info.Client = name
}

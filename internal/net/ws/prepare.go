package ws

import (
	"fmt"
	"net"
	"net/http"
	"time"

	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/broadcast"
	"gitlab.com/Iron_E/networking-final/chat-server/internal/net/ws/handlers/chat"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/dirs"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

/*
 * CONSTANTS
 */

const _CHAT_SOCKET_DIR string = "/chat-server"

/*
 * FUNCTIONS
 */

// Start up the WebSocket server on port number [port].
func Prepare(port int, webDir *dirs.WebDir) (*net.Listener, *http.Server) {// †
	if logger, err := zaputil.NewProduction("Prepare()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		// Create a broker for the server.
		broker := new(broadcast.Broker)

		// Create new multiplexer and define what the multiplexer handles.
		chatMux := http.NewServeMux()
		chatMux.HandleFunc(_CHAT_SOCKET_DIR, chat.NewHandleFunc(broker, webDir))

		if listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port)); err != nil {
			panic(err)
		} else {

			// Start the server
			logger.Info("Server starting. Use any IP address from this device's network interfaces to access it.",
				zap.String( "Address",fmt.Sprintf("ws://%s%s",listener.Addr(),_CHAT_SOCKET_DIR) ),
			)

			// Specifiy the server options.
			server := http.Server{
				Handler: chatMux,
				IdleTimeout:  0 * time.Second, // infinite; we're using a websocket
				ReadTimeout:  0 * time.Second, // infinite; we're using a websocket
				WriteTimeout: 0 * time.Second, // infinite; we're using a websocket
			}

			return &listener, &server
		}
	}
}// ‡

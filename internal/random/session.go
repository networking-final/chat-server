package random

import (
	"encoding/base64"
	"math/rand"
)

// Every character available in the base64 encoding scheme.
const base64Chars string = "abcdefghijklmnopqrstuvwxyz"+"ABCDEFGHIJKLMNOPQRSTUVWXYZ"+"+/"

// Create a random string of [length] matching the definition of <client_session>.
// It is encoded through base64.
func RandomSession(length uint) string {
	// Create a slice of bytes to hold our session.
	// We will be encoding in base64, so it needs to be length*64 bytes long.
	sessionBytes := make([]byte, length)

	for i := range sessionBytes {

		sessionBytes[i] = base64Chars[ rand.Intn(len(base64Chars)) ]
	}

	return base64.RawStdEncoding.EncodeToString(sessionBytes)
}

package shelve

import (
	"errors"
	"sync"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/dirs"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

// Write a [webDir] back to the disk.
func Shelve(webDir *dirs.WebDir, webDirPath string) {
	if logger, err := zaputil.NewProduction("Shelve()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		logFail := func(err error, fileName string) { // †
			if err.Error() != "" {
				logger.Error("Failed to write file. Trying again...",
					zap.String("File", fileName),
					zap.Error(err),
				)
			}
		} // ‡

		// Attempt to write files to the disk, and log errors.
		logger.Info("Writing `webDir` to disk.")
		clientFileErr, queueFileErr, failedMessageCaches, failedCfgFiles := webDir.WriteToDisk(webDirPath)

		// Create a wait group so that the file writes can be retried asyncronously.
		fileWriters := new(sync.WaitGroup)

		if clientFileErr != nil {

			// Keep track of upcoming goroutine.
			fileWriters.Add(1)

			go func() { // †
				defer fileWriters.Done()

				for clientFileErr != nil {

					logFail(clientFileErr, "clients.json")
					clientFileErr = webDir.Clients.WriteToFile(webDirPath)
				}
			}() // ‡
		}

		if queueFileErr != nil {

			// Keep track of upcoming goroutine.
			fileWriters.Add(1)

			go func() { // †
				defer fileWriters.Done()

				for queueFileErr != nil {

					logFail(queueFileErr, "queue.json")
					queueFileErr = webDir.Clients.WriteToFile(webDirPath)
				}
			}() // ‡
		}

		// Loop over all failed RoomDir writes.
		for _, failedId := range failedMessageCaches {

			// Keep track of upcoming goroutine.
			fileWriters.Add(1)

			go func(id marshal.RoomId) { // †
				defer fileWriters.Done()

				if webDir.RoomDirs[id].GetCache() != nil {

					for cacheErr := errors.New(""); cacheErr != nil; {

						logFail(cacheErr, "message_cache.json")
						cacheErr = webDir.RoomDirs[id].GetCache().WriteToFile(webDirPath)
					}

				} else {

					logger.Info("A room's file is nil. Skipping.",
						zap.String("File", "message_cache.json"),
						zap.String("Room ID", string(id)),
					)
				}
			}(failedId) // ‡
		}

		// Loop over all failed RoomDir writes.
		for _, failedId := range failedCfgFiles {

			// Keep track of upcoming goroutine.
			fileWriters.Add(1)

			go func(id marshal.RoomId) { // †
				defer fileWriters.Done()

				if webDir.RoomDirs[id] != nil {

					for cfgErr := errors.New(""); cfgErr != nil; {

						logFail(cfgErr, "room.json")
						cfgErr = webDir.RoomDirs[id].GetCfg().WriteToFile(webDirPath)
					}

				} else {

					logger.Info("A room's file is nil. Skipping.",
						zap.String("File", "room.json"),
						zap.String("Room ID", string(id)),
					)
				}
			}(failedId) // ‡
		}

		// Wait for all writers to complete.
		logger.Info("Waiting for all writes to complete.")
		fileWriters.Wait()
		logger.Info("\tDone waiting for file writes.")
	}
}

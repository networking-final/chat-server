package marshal

import "sync"

/*
 * STRUCTS
 */

// <client> struct.
type Client struct {
	mutController sync.RWMutex
	Name    ClientName    `json:"name"`
	Session ClientSession `json:"session"`
}

/*
 * METHODS
 */

// Get the name of a [Client].
// Using this method is thread safe, over just doing `client.Name`.
func (cl *Client) GetName() ClientName {
	cl.mutController.RLock()
	defer cl.mutController.RUnlock()

	return cl.Name
}

// Get the session of a [Client].
// Using this method is thread safe, over just doing `client.Session`.
func (cl *Client) GetSession() ClientSession {
	cl.mutController.RLock()
	defer cl.mutController.RUnlock()

	return cl.Session
}

// Update the name of the [Client]. Will not set a new name if one has been previously set.
// Returns whether or not the value [newName] was actually set.
// Using this method is thread safe, over just doing `client.Name  = ""`
func (cl *Client) SetName(newName ClientName) bool {
	cl.mutController.Lock()
	defer cl.mutController.Unlock()

	if cl.Name == "" {

		cl.Name = newName
		return true

	} else {

		return false
	}
}

// Update the session of the [Client].
// Using this method is thread safe, over just doing `client.Session = ""`
func (cl *Client) SetSession(newSession ClientSession) {
	cl.mutController.Lock()
	defer cl.mutController.Unlock()

	cl.Session = newSession
}

/*
 * FUNCTIONS
 */

// Create a new [Client]. The fields are unset so that they can be filled with JSON, or manually set later.
func NewClient() *Client {
	return &Client{*new(sync.RWMutex), "", ""}
}

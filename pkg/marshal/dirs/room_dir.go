package dirs

import (
	"sync"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/files"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

/*
 * VARS
 */


/*
 * STRUCTS
 */

// A struct to store a representation of files stored locally in a <room_dir>.
type RoomDir struct {
	mutController sync.RWMutex
	cache *files.MessageCacheDotJson
	cfgFile *files.RoomDotJson
}

/*
 * METHODS
 */

// Thread-safe method to return the `cache` member.
func (rD *RoomDir) GetCache() *files.MessageCacheDotJson {
	rD.mutController.RLock()
	defer rD.mutController.RUnlock()

	return rD.cache
}

// Thread-safe method to return the `cfgFile` member.
func (rD *RoomDir) GetCfg() *files.RoomDotJson {
	rD.mutController.RLock()
	defer rD.mutController.RUnlock()

	return rD.cfgFile
}

// Writes the struct members to the disk.
// If there was an error writing <message_cache.json> to the disk, the first return value is specified.
// If there was an error writing <room.json> to the disk, the first return value is specified.
func (rD *RoomDir) WriteToDisk(webDir string) (error, error) {
	// Don't allow other edits to this struct while we read it.
	rD.mutController.RLock()
	defer rD.mutController.RUnlock()

	if logger, err := zaputil.NewProduction("RoomDir.WriteToDisk()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		logger.Info("Writing to disk.",
			zap.Any("Cache", rD.GetCache()),
			zap.Any("Config File", rD.GetCfg()),
		)

		return rD.GetCache().WriteToFile(webDir), rD.GetCfg().WriteToFile(webDir)
	}
}

/*
 * FUNCTIONS
 */

// Create a [RoomDir] from scratch.
func Room(id marshal.RoomId) *RoomDir {
	rD := new(RoomDir)

	rD.mutController.Lock()
	defer rD.mutController.Unlock()

	rD.cache   = files.MessageCache(id)
	rD.cfgFile = files.Room(id)

	return rD
}

// Read the contents of some <room_dir> into memory.
// If there was an error initializing any part of the struct, an error is returned and all values are returned to nil.
func RoomFromFile(id marshal.RoomId, webDir string) *RoomDir {
	rD := Room(id)
	rD.mutController.Lock()
	defer rD.mutController.Unlock()

	if cache, err := files.MessageCacheFromFile(id, webDir); err != nil {
	
		panic(err)

	} else {

		rD.cache = cache
	}

	if cfgFile, err := files.RoomFromFile(id, webDir); err != nil {

		panic(err)

	} else {

		rD.cfgFile = cfgFile
	}

	return rD
}

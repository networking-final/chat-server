package dirs

import (
	"io/ioutil"
	"sync"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal/files"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

/*
 * STRUCTS
 */

// A struct to store a representation of the files stored locally in the <web_dir>.
type WebDir struct {
	MutController sync.RWMutex
	Clients  *files.ClientsDotJson
	Queue    *files.QueueDotJson
	RoomDirs map[marshal.RoomId]*RoomDir
}

/*
 * METHODS
 */

// Add a new key to [Contents].
func (wD *WebDir) AddRoom(key marshal.RoomId, value *RoomDir) {
	wD.MutController.Lock()
	defer wD.MutController.Unlock()

	if wD.RoomDirs == nil {

		wD.RoomDirs = make(map[marshal.RoomId]*RoomDir)
	}

	wD.RoomDirs[key] = value
}

// Get a value based on the [key] in [Contents].
func (wD *WebDir) Delete(key marshal.RoomId) {
	wD.MutController.Lock()
	defer wD.MutController.Unlock()

	if wD.RoomDirs == nil {

		return
	}

	delete(wD.RoomDirs, key)
}

// Thread-safe method to return the [Clients] member.
func (wD *WebDir) GetClients() *files.ClientsDotJson {
	wD.MutController.RLock()
	defer wD.MutController.RUnlock()

	return wD.Clients
}

// Thread-safe method to return the [Queue] member.
func (wD *WebDir) GetQueue() *files.QueueDotJson {
	wD.MutController.RLock()
	defer wD.MutController.RUnlock()

	return wD.Queue
}

// Thread-safe method to return the [RoomDirs] member.
func (wD *WebDir) GetRoomDirs() map[marshal.RoomId]*RoomDir {
	wD.MutController.RLock()
	defer wD.MutController.RUnlock()

	return wD.RoomDirs
}

// GetRoom a value based on the [key] in [Contents].
func (wD *WebDir) GetRoom(key marshal.RoomId) *RoomDir {
	wD.MutController.RLock()
	defer wD.MutController.RUnlock()

	if wD.RoomDirs == nil {

		return nil
	}

	return wD.RoomDirs[key]
}

// Check whether or not there is a <room_dir> with the specified <room_id> [key].
func (wD *WebDir) RoomExists(key marshal.RoomId) bool {
	wD.MutController.RLock()
	defer wD.MutController.RUnlock()

	if wD.RoomDirs == nil {

		return false
	}

	_, exists := wD.RoomDirs[key]
	return exists
}

// Get all the rooms that a client [name] should be able to interact with.
func (wD *WebDir) RoomsFor(name marshal.ClientName) []marshal.RoomInfo {
	if wD.RoomDirs == nil {

		return nil
	}

	// Don't allow writes to the struct while we read it.
	wD.MutController.RLock()
	defer wD.MutController.RUnlock()

	// Make container for the rooms that are available to the client.
	visibleRooms := make([]marshal.RoomInfo, 0, len(wD.RoomDirs))

	makeRoomInfo := func(roomDotJson *files.RoomDotJson) {
		visibleRooms = append(visibleRooms, marshal.RoomInfo{
			Id: *roomDotJson.GetId(), Name: roomDotJson.Get().GetName(),
		})
	}

	// Iterate over the rooms in the server to find the visible rooms.
	for _, room := range wD.RoomDirs {

		// Grab the <room.json>.
		cfgFile := room.GetCfg()

		// If a room is visible to the client, add it to the slice of visible rooms.
		if !cfgFile.Get().IsPrivate() {

			makeRoomInfo(cfgFile)
			// Don't keep checking if the room is visible.
			continue
		}

		// Assuming the room is private, check if the user is in the list of accepted clients.
		for _, acceptedClientName := range cfgFile.Get().GetClients() {

			// If the client is equal to one of the accepted client names...
			if name == acceptedClientName {

				// ...add it to the list of visible rooms.
				makeRoomInfo(cfgFile)
				// Break from the loop. There's no need to keep looking for names if we just found it.
				break
			}
		}
	}

	return visibleRooms
}

// Writes the struct members to the disk.
// If there was an error writing <clients.json> to the disk, the first return value is specified.
// If there was an error writing <queue.json> to the disk, the first return value is specified.
// Any errors encountered writing <message_cache.json> will be put into the first slice of marshal.RoomId.
// Any errors encountered writing <room.json> will be put into the second slice of marshal.RoomId.
func (wD *WebDir) WriteToDisk(webDir string) (error, error, []marshal.RoomId, []marshal.RoomId) {
	if logger, err := zaputil.NewProduction("WebDir.WriteToDisk()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		// Don't allow writes to this struct while we read it.
		wD.MutController.RLock()
		defer wD.MutController.RUnlock()

		if wD.RoomDirs == nil {

			wD.RoomDirs = make(map[marshal.RoomId]*RoomDir)
		}

		logErr := func(err error) {
			if err != nil {

				logger.Info("...Failed.")

			} else {

				logger.Info("...Succeeded.")
			}
		}

		// Create variables to return later
		logger.Info( "Writing.", zap.String("File", "<clients.json>") )
		clientFileErr := wD.Clients.WriteToFile(webDir)
		logErr(clientFileErr)

		logger.Info( "Writing.", zap.String("File", "<queue.json>") )
		queueFileErr := wD.Queue.WriteToFile(webDir)
		logErr(queueFileErr)

		failedMessageCaches := make( []marshal.RoomId, 0, len(wD.RoomDirs) )
		failedCfgFiles      := make( []marshal.RoomId, 0, len(wD.RoomDirs) )

		// Write all of the rooms and note any errors.
		for id := range wD.RoomDirs {

			logger.Info( "Writing room.",
				zap.String("ID", string(id)),
			)
			cacheErr, cfgErr := wD.RoomDirs[id].WriteToDisk(webDir)

			if cacheErr != nil {

				logger.Info("Failed to write part of room.",
					zap.String("File", "<message_cache.json>"),
					zap.String("ID", string(id)),
				)
				failedMessageCaches = append(failedMessageCaches, id)
			}

			if cfgErr != nil {

				logger.Info("Failed to write part of room.",
					zap.String("File", "<room.json>"),
					zap.String("ID", string(id)),
				)
				failedCfgFiles = append(failedCfgFiles, id)
			}
		}

		return clientFileErr, queueFileErr, failedMessageCaches, failedCfgFiles
	}

}

/*
 * FUNCTIONS
 */

// Create a new [WebDir].
func Web() *WebDir {
	wD := new(WebDir)

	wD.MutController.Lock()
	defer wD.MutController.Unlock()

	wD.Clients  = files.Clients()
	wD.Queue    = files.Queue()

	return wD
}

// Read the contents of the <web_dir> into memory.
// If there was an error initializing any part of the struct, it is returned and all values are returned to nil.
func  WebFromFile(webDir string) (*WebDir, error) {
	if logger, err := zaputil.NewProduction("WebDir.WebFromFile()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		wD := Web()

		wD.MutController.Lock()
		defer wD.MutController.Unlock()

		// Create <clients.json> struct
		if cl, err := files.ClientsFromFile(webDir); err != nil {

			panic(err)

		} else {

			wD.Clients = cl
		}

		// Create <queue.json> struct
		if q, err := files.QueueFromFile(webDir); err != nil {

			panic(err)

		} else {

			wD.Queue = q
		}

		// `ls` the "<web_dir>/rooms"
		if nodes, err := ioutil.ReadDir(files.RoomDirPath(webDir)); err != nil {

			panic(err)

		} else {

			if wD.RoomDirs == nil {

				logger.Info("Struct member was nil.",
					zap.String("Struct", "WebDir"),
					zap.String("Member", "RoomDirs"),
				)
				wD.RoomDirs = make(map[marshal.RoomId]*RoomDir)
			}

			for _, node := range nodes {

				if node.IsDir() {

					// Get the room id
					roomId := marshal.RoomId(node.Name())

					// Map the directory.
					wD.RoomDirs[roomId] = RoomFromFile(roomId, webDir)
				}
			}
		}

		// No errors we want to report were encountered, so return nil.
		return wD, nil
	}
}

package files

import (
	"encoding/json"
	"sync"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

/*
 * STRUCTS
 */

// What type <clients.json> is.
type ClientsDotJson struct {
	mutController sync.RWMutex
	contents marshal.ClientSessionMaps
}

/*
 * METHODS
 */

// Get a value based on the [key] in [Contents].
func (cl *ClientsDotJson) Delete(key marshal.ClientName) {
	cl.mutController.Lock()
	defer cl.mutController.Unlock()

	if cl.contents == nil {
		return
	}

	delete(cl.contents, key)
}


// Get a value based on the [key] in [Contents].
func (cl *ClientsDotJson) GetSession(key marshal.ClientName) marshal.ClientSession {
	cl.mutController.RLock()
	defer cl.mutController.RUnlock()

	if cl.contents == nil {

		return marshal.ClientSession("")
	}

	// Have to explicitly store [exists] in order for the boolean to be saved.
	return cl.contents[key]
}

// Names the list of clients connected to the server.
func (cl *ClientsDotJson) Names() marshal.ClientNames {
	cl.mutController.RLock()
	defer cl.mutController.RUnlock()

	if cl.contents == nil {
		return nil
	}

	clients := make(marshal.ClientNames, len(cl.contents))

	for name, _ := range cl.contents {

		clients = append(clients, name)
	}

	return clients
}

// Put a new key to [Contents].
func (cl *ClientsDotJson) Put(key marshal.ClientName, value marshal.ClientSession) {
	cl.mutController.Lock()
	defer cl.mutController.Unlock()

	if cl.contents == nil {

		cl.contents = make(marshal.ClientSessionMaps)
	}

	cl.contents[key] = value
}

// Check whether or not a client [key] exists in the session map.
func (cl *ClientsDotJson) SessionExists(key marshal.ClientName) bool {
	cl.mutController.RLock()
	defer cl.mutController.RUnlock()

	if cl.contents == nil {

		return false
	}

	// Have to explicitly store [exists] in order for the boolean to be saved.
	_, exists := cl.contents[key]
	return exists
}

// Write a [ClientsDotJson] to a file.
func (cl *ClientsDotJson) WriteToFile(webDir string) error {
	cl.mutController.RLock()
	defer cl.mutController.RUnlock()

	if logger, err := zaputil.NewProduction("ClientsDotJson.WriteToFile"); err != nil {

		panic(err)

	} else {
		defer logger.Sync()

		logger.Info("Attempting to write to file.")

		if err = writeWebDirJson(cl.contents, "clients", webDir); err != nil {

			logger.Error( "Failed.",zap.Error(err) )
			return err

		} else {

			logger.Info("Success.")
			return nil
		}
	}
}

/*
 * FUNCTIONS
 */

// Create an instance of [ClientsDotJson] with default values.
func Clients() *ClientsDotJson {
	cl := new(ClientsDotJson)

	cl.mutController.Lock()
	defer cl.mutController.Unlock()

	return cl
}

// Load Maps from "<web_dir>/<clients.json>"
func ClientsFromFile(webDir string) (*ClientsDotJson, error) {
	if logger, err := zaputil.NewProduction("ClientsFromFile()"); err != nil {

		panic(err)

	} else {
		cl := Clients()

		cl.mutController.Lock()
		defer cl.mutController.Unlock()

		if jsonBytes, err := initWebDirJson("clients",webDir); err != nil {

			logger.Error("There was an error unloading JSON.",
				zap.Error(err),
			)

		} else if !marshal.JsonIsNull(string(jsonBytes)) {

			if cl.contents == nil {

				cl.contents = make(marshal.ClientSessionMaps)
			}

			marshalErr := json.Unmarshal(jsonBytes, &cl.contents)
			return cl, marshalErr

		}

		return cl, nil
	}
}

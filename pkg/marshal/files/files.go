package files

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

/*
 * FUNCTIONS
 */

func RoomDirPath(webDir string) string {
	return path.Join(webDir, "/rooms")
}

// Reads some JSON file and returns its raw byte contents.
func readDotJson(path string) ([]byte, error) {
	cfg := zap.NewProductionConfig()
	cfg.InitialFields = map[string]interface{}{
		"File path": path,
		"Func name": "readDotJson()",
	}
	cfg.OutputPaths = []string{zaputil.OutPath}
	if logger, err := cfg.Build(); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		// Open file for reading.
		logger.Info("Opening file.")
		if fileInput, err := os.Open(path); err != nil {

			return nil, err

		} else {

			// Define function to close file after [readDotJson()] finishes.
			defer func() {
				if err := fileInput.Close(); err != nil {
					panic(err)
				}
				logger.Info("Closing file.")
			}()

			// Define a reader for the file.
			fiReader := bufio.NewReader(fileInput)

			// Read while until end.
			logger.Info("Reading file.")
			return ioutil.ReadAll(fiReader)
		}
	}
}

// Writes to some JSON file and returns whether or not there were any errors.
func writeDotJson(path string, jsonBytes []byte) error {
	cfg := zap.NewProductionConfig()
	cfg.InitialFields = map[string]interface{}{
		"File path": path,
		"Func name": "writeDotJson()",
	}
	cfg.OutputPaths = []string{zaputil.OutPath}
	if logger, err := zaputil.NewProduction("writeDotJson()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		// If the file exists
		logger.Info("Checking if file exists.")
		if _, err := os.Stat(path); os.IsExist(err) {

			// Remove the file
			logger.Info("File exists. Removing...")
			if err := os.Remove(path); err != nil {

				// Return any errors
				return err
			}
			logger.Info("Removed file.")
		}

		// Open the file
		filePtr, err := os.Create(path)
		logger.Info("Creating file.")
		// Define function to close the file after [writeDotJson()] finishes.
		defer func() {
			// Assure that there is a file to close
			if filePtr != nil {
				logger.Info("Closing")
				if err := filePtr.Close(); err != nil {
					panic(err)
				}
				logger.Info("...closed.")
			}
		}()

		// Check to make sure we haven't had an error yet.
		if err != nil {
			return err
		}

		// Define writer for the file.
		fileWriter := bufio.NewWriter(filePtr)

		// Write to the file and save errors to [err]
		logger.Info("Writing to file.")
		if bytesWritten, err := fmt.Fprintln( fileWriter,string(jsonBytes) ); err != nil {
			// Return the error if there is one.
			return err
		// If the number of bytes written is less than the number we wanted to write, throw a new error.
		} else if bytesWritten < len(jsonBytes) {
			return fmt.Errorf("Fewer bytes written than expected")
		// If everything was successful, flush the contents (which may ALSO error.)
		} else {
			logger.Info("Flushing.")
			return fileWriter.Flush()
		}
	}
}

package files

import (
	"encoding/json"
	"errors"
	"math"
	"sync"

	"gitlab.com/Iron_E/networking-final/chat-server/internal/random"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

/*
 * STRUCTS
 */

// The <message_cache.json> struct.
type MessageCacheDotJson struct {
	mutController sync.RWMutex
	contents  marshal.MessageIdMaps
	id       *marshal.RoomId
}

/*
 * METHODS
 */

// Get a value based on the [key] in [ContentPtr].
func (mC *MessageCacheDotJson) Delete(key marshal.MessageId) {
	mC.mutController.Lock()
	defer mC.mutController.Unlock()

	if mC.contents == nil {

		return
	}

	delete(mC.contents, key)
}

// Get a value based on the [key] in [ContentPtr].
func (mC *MessageCacheDotJson) Get(key marshal.MessageId) *marshal.Message {
	mC.mutController.RLock()
	defer mC.mutController.RUnlock()

	if mC.contents == nil {

		return new(marshal.Message)
	}

	return mC.contents[key]
}

// Check whether or not there are any cached [Message]s with the provided [id].
func (mC *MessageCacheDotJson) IdCached(id marshal.MessageId) bool {
	mC.mutController.RLock()
	defer mC.mutController.RUnlock()

	if mC.contents == nil {

		return false
	}

	_, exists := mC.contents[id]
	return exists
}

// Add a new message to the <message_cache.json>.
// Returns the <message_id> of the newly added <message>.
func (mC *MessageCacheDotJson) Put(value *marshal.Message) (marshal.MessageId, error) {
	mC.mutController.Lock()
	defer mC.mutController.Unlock()

	if mC.contents == nil {

		mC.contents = make(marshal.MessageIdMaps)
	}

	const MESSAGE_ID_SIZE uint8 = 25;

	// The maximum number of messages— determined by the charset of "chat-server/random" and the [MESSAGE_ID_SIZE].
	maxLength := math.Pow( 36,float64(MESSAGE_ID_SIZE) )

	// If the <message_cache.json> contains too many keys, the following `for` block will freeze the program.
	if float64(len(mC.contents)) < maxLength {

		// Loop until an id is assigned.
		for {

			// Generate a random ID.
			randId := marshal.MessageId( random.RandomSession(uint(MESSAGE_ID_SIZE)) )

			// Make sure the random ID is actually random.
			if _, exists := mC.contents[randId]; !exists {

				mC.contents[randId] = value
				return randId, nil
			}
		}
	
	} else {

		return "", errors.New("The <message_cache.json> is full.")
	}
}

// Write [r.RoomContents] to file.
func (mC *MessageCacheDotJson) WriteToFile(webDir string) error {
	return writeRoomDirJson(mC.contents, "message_cache", *mC.id, webDir)
}

/*
 * FUNCTIONS
 */

// Create a new [MessageCacheDotJson] from [id].
func MessageCache(id marshal.RoomId) *MessageCacheDotJson {
	mC := new(MessageCacheDotJson)

	mC.mutController.Lock()
	defer mC.mutController.Unlock()

	 mC.contents = make(marshal.MessageIdMaps)
	 mC.id       = new(marshal.RoomId)
	*mC.id       = id

	return mC
}

// Load Maps from "<web_dir>/rooms/<room_id>/<message_cache.json>"
func MessageCacheFromFile(id marshal.RoomId, webDir string) (*MessageCacheDotJson, error) {
	if logger, err := zaputil.NewProduction("MessageCacheFromFile()"); err != nil {

		panic(err)

	} else {
		mC := MessageCache(id)

		mC.mutController.Lock()
		defer mC.mutController.Unlock()

		if jsonBytes, err := initRoomDirJson("message_cache", *mC.id, webDir); err != nil {

			logger.Error("There was an issue unloading json.",
				zap.Error(err),
			)

		} else if !marshal.JsonIsNull( string(jsonBytes) ) {

			if mC.contents == nil {

				mC.contents = make(marshal.MessageIdMaps)
			}

			jsonErr := json.Unmarshal(jsonBytes, &mC.contents)
			return mC, jsonErr
		
		}

		return mC, nil
	}
}

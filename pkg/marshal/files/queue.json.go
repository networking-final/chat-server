package files

import (
	"encoding/json"
	"sync"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

/*
 * QUEUE
 */

// What type <queue.json> is.
type QueueDotJson struct {
	MutController sync.RWMutex
	Contents marshal.Queue
}

/*
 * METHODS
 */

// Check whether or not there are any messages queued for a given client `name`.
func (q *QueueDotJson) IsMsgForClient(name marshal.ClientName) bool {
	q.MutController.RLock()
	defer q.MutController.RUnlock()

	if q.Contents == nil {

		return false
	}

	_, exists := q.Contents[name]
	return exists
}

// Get a value based on the [key] in [Contents].
func (q *QueueDotJson) Get(key marshal.ClientName) marshal.MessagesQueued {
	q.MutController.RLock()
	defer q.MutController.RUnlock()

	if q.Contents == nil {

		return nil
	}


	return q.Contents[key]
}

// Put a new key to [Contents].
func (q *QueueDotJson) Put(client marshal.ClientName, roomId marshal.RoomId, msgId marshal.MessageId) {
	q.MutController.Lock()
	defer q.MutController.Unlock()

	// Make sure main list is initialized
	if q.Contents == nil {

		q.Contents = make(marshal.Queue)
	}

	// Make sure inner map is initialized
	if q.Contents[client] == nil {

		q.Contents[client] = make(marshal.MessagesQueued)
	}

	// Make sure inner list is initialized
	if q.Contents[client][roomId] == nil {

		q.Contents[client][roomId] = make([]marshal.MessageId, 0)
	}

	q.Contents[client][roomId] = append(q.Contents[client][roomId], msgId)
}


// Get a value based on the [key] in [Contents].
func (q *QueueDotJson) RemoveFirst(client marshal.ClientName, room marshal.RoomId) {
	q.MutController.Lock()
	defer q.MutController.Unlock()

	// Make sure everything checks out before trying to remove a message from something that doesn't exist.
	if q.Contents != nil && q.Contents[client] != nil && q.Contents[client][room] != nil {

		if len(q.Contents[client][room]) > 1 {

			// Break the slice apart and put it back together without the message ID that must be removed.
			q.Contents[client][room] = q.Contents[client][room][1:]

		} else {

			// Reset the slice.
			q.Contents[client][room] = make([]marshal.MessageId, 0)
		}
	}
}

// Write a [QueueDotJson] back to the disk.
func (q *QueueDotJson) WriteToFile(webDir string) error {
	return writeWebDirJson(q.Contents, "queue", webDir)
}

/*
 * FUNCTIONS
 */

// Create a new queue.
func Queue() *QueueDotJson {
	q := new(QueueDotJson)

	q.MutController.Lock()
	defer q.MutController.Unlock();

	return q
}

// Create a new queue from the directory [webDir] specified.
func QueueFromFile(webDir string) (*QueueDotJson, error) {
	if logger, err := zaputil.NewProduction("QueueFromFile()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		q := Queue()

		q.MutController.Lock()
		defer q.MutController.Unlock()

		if jsonBytes, err := initWebDirJson("queue", webDir); err != nil {

			logger.Error("There was an issue unloading JSON.",
				zap.Error(err),
			)

		} else if !marshal.JsonIsNull( string(jsonBytes) ) {

			if q.Contents == nil {

				q.Contents = make(marshal.Queue)
			}

			jsonErr := json.Unmarshal(jsonBytes, &q.Contents)
			return q, jsonErr

		}

		return q, nil
	}
}

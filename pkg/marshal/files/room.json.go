package files

import (
	"encoding/json"
	"sync"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

// The <room.json> struct.
type RoomDotJson struct {
	mutController sync.RWMutex
	contents      *marshal.Room
	id            *marshal.RoomId
}

/*
 * IMPLEMENTATIONS
 */

// Get a value based on the [key] in [ContentPtr].
func (r *RoomDotJson) Get() *marshal.Room {
	r.mutController.RLock()
	defer r.mutController.RUnlock()

	return r.contents
}

// Thread-safe method to get the `id` field.
func (r *RoomDotJson) GetId() *marshal.RoomId {
	r.mutController.RLock()
	defer r.mutController.RUnlock()

	return r.id
}

// Set a new value to [Contents].
func (r *RoomDotJson) Set(value *marshal.Room) {
	r.mutController.Lock()
	defer r.mutController.Unlock()

	r.contents = value
}

// Write [r.RoomContents] to file.
func (r *RoomDotJson) WriteToFile(webDir string) error {
	return writeRoomDirJson(r.contents, "room", *r.id, webDir)
}

/*
 * FUNCTIONS
 */

// Create a new [RoomDotJson] from [id].
func Room(id marshal.RoomId) *RoomDotJson {
	r := new(RoomDotJson)

	r.mutController.Lock()
	defer r.mutController.Unlock()

	r.contents = new(marshal.Room)
	r.id = new(marshal.RoomId)
	*r.id = id

	return r
}

// Load Maps from "<web_dir>/rooms/<room_id>/<room.json>"
func RoomFromFile(id marshal.RoomId, webDir string) (*RoomDotJson, error) {
	if logger, err := zaputil.NewProduction("QueueFromFile()"); err != nil {

		panic(err)

	} else {
		r := Room(id)

		r.mutController.Lock()
		defer r.mutController.Unlock()

		logger.Info("Reading <room.json>")
		if jsonBytes, err := initRoomDirJson("room", *r.id, webDir); err != nil {

			logger.Error("There was an issue unloading JSON.",
				zap.Error(err),
			)

		} else if !marshal.JsonIsNull(string(jsonBytes)) {

			jsonErr := json.Unmarshal(jsonBytes, &r.contents)
			logger.Debug("Read <room.json>.",
				zap.String("Room Name", string(r.contents.GetName())),
			)

			if jsonErr != nil {

				logger.Error("There was an error unmarshalling <room.json>.",
					zap.Error(jsonErr),
				)
			}

			return r, jsonErr
		}

		logger.Info("<room.json> file was null.")
		return r, nil
	}
}

// Create a new [Room] using [id] and [contents].
func RoomWithContents(contents *marshal.Room, id marshal.RoomId) *RoomDotJson {
	room := Room(id)
	room.Set(contents)

	return room
}

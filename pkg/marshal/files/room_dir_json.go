package files

import (
	"encoding/json"
	"fmt"
	"path"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"
)

/*
 * FUNCTIONS
 */

// Gets the full path of some [roomDirJson]
func getFullPath(fileName string, id marshal.RoomId, webDir string) string {
	return path.Join( RoomDirPath(webDir),string(id),fmt.Sprintf("%s.json",fileName) )
}

// Initializes a [roomDirJson] from its file.
// File is calculated using [fileName] and [id].
func initRoomDirJson(fileName string, id marshal.RoomId, webDir string) ([]byte, error) {
	return readDotJson( getFullPath(fileName,id,webDir) )
}

// Writes a [roomDirJson] to its file.
// File is calculated using [fileName] and [id].
func writeRoomDirJson(contents interface{}, filePath string, id marshal.RoomId, webDir string) error {
	if jsonBytes, err := json.Marshal(contents); err != nil {
		return err
	} else {
		return writeDotJson( getFullPath(filePath,id,webDir), jsonBytes )
	}
}

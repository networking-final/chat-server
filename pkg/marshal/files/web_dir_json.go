package files

import (
	"encoding/json"
	"fmt"
	"path"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

/*
 * FUNCTIONS
 */

// Initializes a [webDirJson] from its file.
// File is calculated using [fileName] and [webDir].
func initWebDirJson(fileName string, webDir string) ([]byte, error) {
	return readDotJson( path.Join(webDir,fmt.Sprintf("%s.json", fileName)) )
}

// Writes a [webDirJson] to its file.
// File is calculated using [fileName] and [id].
func writeWebDirJson(contents interface{}, filePath string, webDir string) error {
	if logger, err := zaputil.NewProduction("writeWebDirJson()"); err != nil {
		panic(err)
	} else {
		defer logger.Sync()

		logger.Info("Attempting to marshal JSON.")
		if jsonBytes, err := json.Marshal(contents); err != nil {
			logger.Error("...Failed.", zap.Error(err))
			return err
		} else {
			logger.Info("...Succeeded.")
			return writeDotJson( path.Join(webDir,fmt.Sprintf("%s.json", filePath)), jsonBytes )
		}
	}
}

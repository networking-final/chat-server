package marshal

import "strings"

// Check whether some JSON value is empty.
func JsonIsNull(jsonStr string) bool {
	return len(jsonStr) < 1 || strings.EqualFold( jsonStr,"null" ) || strings.EqualFold( jsonStr,"{}" )
}

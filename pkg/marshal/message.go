package marshal

import (
	"sync"
)

/*
 * STRUCTS
 */

// <message> struct.
type Message struct {
	mutController sync.RWMutex
	Body    MessageBody `json:"body"`
	Client  ClientName  `json:"client"`
	Time    string      `json:"time"`
}

// Thread-safe method to return the [Body] member.
func (m *Message) GetBody() MessageBody {
	m.mutController.RLock()
	defer m.mutController.RUnlock()

	return m.Body
}

// Thread-safe method to get the [Client] member.
func (m *Message) GetClient() ClientName {
	m.mutController.RLock()
	defer m.mutController.RUnlock()

	return m.Client
}

// Thread-safe method to get the [Time] member.
func (m *Message) GetTime() string {
	m.mutController.RLock()
	defer m.mutController.RUnlock()

	return m.Time
}

// Thread-safe method to set the [Client] member.
func (m *Message) SetClient(newClient ClientName) {
	m.mutController.Lock()
	defer m.mutController.Unlock()

	m.Client = newClient
}

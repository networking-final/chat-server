package marshal

import "sync"

/*
 * STRUCTS
 */

// <message_dequeued> struct.
type MessageDequeued struct {
	mutController sync.RWMutex
	Msg *Message `json:"message"`
	Id  RoomId  `json:"room"`
}

/*
 * METHODS
 */

// Thread-safe Msg field getter.
func (mD *MessageDequeued) GetMsg() *Message {
	mD.mutController.RLock()
	defer mD.mutController.RUnlock()

	return mD.Msg
}

// Thread-safe Msg field getter.
func (mD *MessageDequeued) GetId() RoomId {
	mD.mutController.RLock()
	defer mD.mutController.RUnlock()

	return mD.Id
}

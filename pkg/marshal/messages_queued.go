package marshal

/*
 * ALIASES
 */

// What type <messages_queued> are
type MessagesQueued map[RoomId][]MessageId

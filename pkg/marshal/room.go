package marshal

import (
	"sync"
)

/*
 * STRUCTS
 */

type Room struct {
	mutController sync.RWMutex
	Clients   ClientNames `json:"clients"`
	Name      RoomName    `json:"name"`
	Private   bool        `json:"private"`
}

/*
 * METHODS
 */

// Add a [name] to the [Clients] list.
func (r *Room) AppendClient(name ClientName) {
	r.mutController.Lock()
	defer r.mutController.Unlock()

	if r.Clients == nil {

		r.Clients = make(ClientNames, 1)
	}

	r.Clients = append(r.Clients, name)
}

// Thread-safe method to get the [Client] field.
func (r *Room) GetClient(index int) ClientName {
	r.mutController.RLock()
	defer r.mutController.RUnlock()

	if r.Clients == nil || len(r.Clients) < index-1 {

		return ClientName("")

	} else {

		return r.Clients[index]
	}

}

// Thread-safe method to get the [Clients] field.
func (r *Room) GetClients() ClientNames {
	r.mutController.RLock()
	defer r.mutController.RUnlock()

	return r.Clients
}

// Thread-safe method to get the [Private] field.
func (r *Room) IsPrivate() bool {
	r.mutController.RLock()
	defer r.mutController.RUnlock()

	return r.Private
}

// Thread-safe method to get the [Name] field.
func (r *Room) GetName() RoomName {
	r.mutController.RLock()
	defer r.mutController.RUnlock()

	return r.Name
}

// Thread-safe method to get the [Clients] from this room and see if `name` is in it.
func (r *Room) VisibleToClient(name ClientName) bool {
	r.mutController.RLock()
	defer r.mutController.RUnlock()

	for _, clientName := range r.Clients {

		if clientName == name {

			return true
		}
	}

	return false
}

/*
 * FUNCTIONS
 */

// Create a room from scratch, providing all the members.
func room(clientNames ClientNames, name RoomName, private bool) *Room {
	r := new(Room)

	r.Clients   = clientNames
	r.Name      = name
	r.Private = private

	return r
}

// Create a room where [Private] is set to `false`.
func RoomPublic(name RoomName) *Room {
	return room( make(ClientNames,0) , name , false )
}

// Create a room where [Private] is set to `true`.
func RoomPrivate(clientNames ClientNames, name RoomName) *Room {
	return room(clientNames, name, true)
}

package marshal

// The <room_info> struct.
type RoomInfo struct {
	Id   RoomId   `json:"id"`
	Name RoomName `json:"name"`
}

package socket_op

/*
 * CONSTANTS
 */

const (
	// <content_type> for <client>.
	TYPE_CLIENT         string = "client"
	// <content_type> for <client_session>.
	TYPE_CLIENT_SESSION string = "client_session"
	// <content_type> for <message>.
	TYPE_MSG            string = "message"
	// <content_type> for <message_dequeued>.
	TYPE_QUEUE         string = "queue"
	// <content_type> for <rooms>.
	TYPE_ROOMS          string = "rooms"

	OP_TYPE_REQUEST      string = "get"
	OP_TYPE_ACKNOWLEDGE  string = "ok"
	OP_TYPE_RESPONSE     string = "post"
)

/*
 * VARS
 */

var (
	Fetch *Operation = NewAcknowledgement("fetch")
)

/*
 * STRUCTS
 */

type Operation struct {
	ContentType string `json:"content_type"`
	OpType      string `json:"op_type"`
}

/*
 * FUNCTIONS
 */


func IsOperation(jsonMap map[string]interface{}) bool {
	if _, keyExists := jsonMap["op_type"]; keyExists {
		return true
	} else {
		return false
	}
}

func operation(contentType string, opType string) *Operation {
	o := new(Operation)

	o.ContentType = contentType
	o.OpType      = opType

	return o
}

// Create a new Acknowledgement from some ContentType
func NewAcknowledgement(contentType string) *Operation {
	return operation(contentType, "ok")
}

// Create a new Request from some ContentType
func NewRequest(contentType string) *Operation {
	return operation(contentType, "get")
}

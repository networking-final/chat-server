package socket_op

/*
 * STRUCT
 */

// This defines a Response to a request Operation.
type Response struct {
	Content *interface{} `json:"content"`
	Op      *Operation   `json:"operation"`
}

/*
 * FUNCTIONS
 */

// Tests whether or not some `map` is a [response].
func IsResponse(jsonMap map[string]interface{}) bool {
	if _, hasKey := jsonMap["operation"]; hasKey {
		return true
	} else {
		return false
	}
}

// Create a new Response from some Content and a ContentType specification.
func NewResponse(content interface{}, contentType string) *Response {
	r := new(Response)

	 r.Content = new(interface{})
	*r.Content = content
	 r.Op      = operation(contentType, "post")

	return r
}


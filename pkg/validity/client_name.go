package validity

import "gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"

var validClientNameRunes []rune = append(
	// The base []rune
	validMessageIdRunes,
	// The additional symbols
	'-', '_', '.',
)

// Check if [toTest] is a valid <message_id>.
// All `rune`s in [toTest] must be in [validMessageIdRunes].
func TestForValidClientName(toTest marshal.ClientName) bool {
	return validRuneSet(string(toTest), validClientNameRunes)
}

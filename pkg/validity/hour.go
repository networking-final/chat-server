package validity

// Check if [toTest] is a valid <hour>.
// Must be 2 `rune`s long and be between the numbers 0 thru (and including) 23.
func TestForValidHour(toTest string) bool {
	if validNumberWithinRange(toTest, 0, 23) && len(toTest) == 2 {
		return true
	}
	return false
}

package validity

import "gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"

var validMessageIdRunes []rune = []rune{
	// Numbers
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
	// Lower case alphabet
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
	// Upper case alphabet
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
}

// Check if [toTest] is a valid <message_id>.
// All `rune`s in [toTest] must be in [validMessageIdRunes].
func TestForValidMessageId(toTest marshal.MessageId) bool {
	return validRuneSet(string(toTest), validMessageIdRunes)
}

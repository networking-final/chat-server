package validity

// Check if [toTest] is a valid <minute>.
// Must be 2 `rune`s long and be between the numbers 0 thru (and including) 59.
func TestForValidMinute(toTest string) bool {
	if validNumberWithinRange(toTest, 0, 59) && len(toTest) == 2 {
		return true
	}
	return false
}

package validity

// Check if [toTest] is a valid <month>.
// Must be 2 `rune`s long and be between the numbers 1 thru (and including) 12.
func TestForValidMonth(toTest string) bool {
	if validNumberWithinRange(toTest, 1, 12) && len(toTest) == 2 {
		return true
	}
	return false
}

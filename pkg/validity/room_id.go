package validity

import "gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"

// Check if [toTest] is a valid <room_id>.
// All `rune`s in [toTest] must be in [validMessageIdRunes].
func TestForValidRoomId(toTest marshal.RoomId) bool {
	return TestForValidMessageId( marshal.MessageId(toTest) )
}

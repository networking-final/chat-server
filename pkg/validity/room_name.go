package validity

import "gitlab.com/Iron_E/networking-final/chat-server/pkg/marshal"

// Check if [toTest] is a valid <message_id>.
// All `rune`s in [toTest] must be in [validMessageIdRunes].
func TestForValidRoomName(toTest marshal.RoomName) bool {
	return TestForValidClientName( marshal.ClientName(toTest) )
}

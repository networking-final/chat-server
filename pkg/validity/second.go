package validity

// Check if [toTest] is a valid <second>.
// Must be 2 `rune`s long and be between the numbers 0 thru (and including) 59.
func TestForValidSecond(toTest string) bool {
	return TestForValidMinute(toTest)
}

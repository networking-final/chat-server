package validity

import (
	"fmt"
	"strconv"

	"gitlab.com/Iron_E/networking-final/chat-server/pkg/zaputil"
	"go.uber.org/zap"
)

/*
 * VARS
 */

var InvalidError error = fmt.Errorf("Data has tested invalid.")

/*
 * METHODS
 */

// Test if the length of the [toTest] string is within the specified range.
func validLength(toTest string, startNum int, inclusiveEndNum int) bool {
	return validRange(
		len(toTest),
		startNum,
		inclusiveEndNum,
	)
}

// Tests if some `string` [toTest] can be converted to a number that falls within the range of [startNum] and [inclusiveEndNum]
func validNumberWithinRange(toTest string, startNum int, inclusiveEndNum int) bool {
	// Try converting [toTest] to an `int`.
	if toTestAsInt, err := strconv.Atoi(toTest); err != nil {
		// If it failed, return false.
		return false
	} else {
		// Make sure the `int` is within the range specified.
		if validRange(toTestAsInt, startNum, inclusiveEndNum) {
			// If it is return true.
			return true
		}
	}
	// Unless the function has already returned true, return false.
	return false
}

// Assure [rangePoint] is numerically within [startNum] and [inclusiveEndNum]
func validRange(rangePoint int, startNum int, inclusiveEndNum int) bool {
	return startNum <= rangePoint && rangePoint <= inclusiveEndNum
}

func validRuneSet(toTest string, validRunes []rune) bool {
	if logger, err := zaputil.NewProduction("validRuneSet()"); err != nil {

		panic(err)

	} else {
		defer logger.Sync()

		// Range over all the `rune`s in [toTest].
		for _, toTestRune := range toTest {

			logger.Info( "Testing rune.",
				zap.String("Rune", string(toTestRune)),
			)

			// Create var for if [toTest] is one of the [validRunes].
			toTestRuneInValidRunes := false

			// Range over [validRunes]
			for _, validRune := range validRunes {

				// If [toTestRune] is equal to [validRune]...
				if toTestRune == validRune {

					// ...then [toTestRune] is valid.
					logger.Info( "Rune is VALID.",
						zap.String("Rune", string(toTestRune)),
					)
					toTestRuneInValidRunes = true

					// No need to keep checking.
					break
				}
			}

			// If [toTestRune] was not valid, that means that the whole [toTest] is invalid.
			if !toTestRuneInValidRunes {

				logger.Info( "Rune is INVALID. Ending test.",
					zap.String("Rune", string(toTestRune)),
				)
				return false
			}
		}
		// The function never returned false, so it must be true.
		return true

	}

}


package validity

import "strconv"

// Check if [toTest] is a valid <year>.
// Must be greater than-or-equal to 2020.
func TestForValidYear(toTest string) bool {
	if toTestAsInt, err := strconv.Atoi(toTest); err != nil {
		// Must be a valid number
		return false
	} else {
		// 2020 was the year the application was written, so year generated by any clients should be smaller than that.
		if toTestAsInt >= 2020 {return true} else {return false}
	}
}

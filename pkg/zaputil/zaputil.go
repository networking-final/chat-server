package zaputil

import "go.uber.org/zap"

const OutPath string = "log.json"

// Create a [zap.NewProduction()] with the specified function name [funcName].
func NewProduction(funcName string) (*zap.Logger, error) {
	cfg := zap.NewProductionConfig()
	cfg.InitialFields = map[string]interface{}{
		"Function": funcName,
	}
	cfg.OutputPaths = []string{OutPath}

	return cfg.Build()
}
